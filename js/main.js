$(document).ready(function($) {
    $('#fullpage').fullpage({
        verticalCentered: true,
        autoScrolling: false,
        menu: '#top-bar',
        paddingTop: '80px',
        responsiveWidth: 845,
        anchors:['inicio', 'contacto'],
        afterLoad: function(anchorLink, index){
            console.log(anchorLink)
            if (anchorLink=='inicio') {
              $('.iphone-wrapper').animate({
                'top': '0px'},
                500,function() {
                    $('.iphone-black').animate({'left': '0px'},500);
                    $('.iphone-white').animate({'left': '230px'},500);
                });
              $('#top-bar').addClass('main-top'); 
              $('#top-bar').removeClass('contact-top');
                  $('.contacto').css({
                  display: 'block'
              });
              $('.inicio').css({
                  display: 'none'
              });
            }else if (anchorLink=='contacto'){
                $('.iphone-wrapper-contact').animate({
                'top': '0px'},
                0,function() {
                    $('.iphone-black-perspective').animate({'left': '-100px'},500);
                    $('.iphone-gold-perspective').animate({'left': '30px'},500);
                });
                $('#top-bar').removeClass('main-top'); 
                $('#top-bar').addClass('contact-top');
                $('.contacto').css({
                  display: 'none'
                });
                $('.inicio').css({
                  display: 'block'
                });
            }
        },
        onLeave: function(index, nextIndex, direction){
            if (index==1) {
                $('.iphone-wrapper').animate({
                'top': '800px'},
                500,function() {
                    $('.iphone-black').animate({'left': '120px'},0);
                    $('.iphone-white').animate({'left': '120px'},0);
                });
            }else if(index==2){
                $('.iphone-wrapper-contact').animate({
                'top': '0px'},
                0,function() {
                    $('.iphone-black-perspective').animate({'left': '100px'},500);
                    $('.iphone-gold-perspective').animate({'left': '0px'},500);
                });
            }
        }
    });
});

angular
.module('LandingCrayonStarApp', [])
.factory("transformRequestAsFormPost", function () {
    function transformRequest(data, getHeaders) {
        var headers = getHeaders();
        headers["Content-type"] = "application/x-www-form-urlencoded; charset=utf-8";
        return (serializeData(data));
    }
    return (transformRequest);

    function serializeData(data) {
        if (!angular.isObject(data)) {
            return ((data == null) ? "" : data.toString());
        }
        var buffer = [];
        // Serialize each key in the object.
        for (var name in data) {
            if (!data.hasOwnProperty(name)) {
                continue;
            }
            var value = data[name];
            buffer.push(encodeURIComponent(name) + "=" + encodeURIComponent((value == null) ? "" : value));
        }
        // Serialize the buffer and clean it up for transportation.
        var source = buffer.join("&").replace(/%20/g, "+");
        return (source);
    }
})
.controller('ContactCtrl', ['$scope','$timeout','$http','transformRequestAsFormPost', function ($scope,$timeout,$http,transformRequestAsFormPost) {
    $scope.formContact = {};
    $scope.error = {};
    $scope.labelBtn = "Enviar";
    
    $scope.sendFormContact = function () {
        $scope.error = {};
        if ($scope.formContact.name) {
            if ($scope.formContact.email) {
                if ($scope.formContact.budget) {
                    if ($scope.formContact.message) {
                        $scope.formContact.affair = "Contacto para Proyecto - LandingPage Crayon Star"
                        console.log($scope.formContact)
                        $scope.labelBtn = "Enviando...";
                        $http({
                          method: 'POST',
                          url: 'https://app.crayonstar.com/beta1/api/mailcontact/',
                          data: $scope.formContact,
                          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                          transformRequest: transformRequestAsFormPost
                        }).success(function(response) {
                            $scope.labelBtn = "Enviar";
                            $scope.msgSuccess = response.msg;
                            $scope.formContact = {};
                            $timeout(function() {
                                $scope.msgSuccess = "";
                            },3000)
                        })
                    }else{
                        $scope.error.message = 'Ingrese la descripción del proyecto, por favor.';
                    }
                }else{
                    $scope.error.budget = 'Defina un presupuesto, por favor.';
                }
            }else{
                $scope.error.email = 'Ingrese un Correo Electrónico, por favor.';
            }
        }else{
            $scope.error.name = 'Ingrese un Nombre y Apellido, por favor.';
        }
    }    
}]);
