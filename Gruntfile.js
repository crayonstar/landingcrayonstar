module.exports = function (grunt) {
    'use strict';
    // Project configuration
    grunt.initConfig({
        // Task configuration
        concat: {
            dist: {
                src: [
                'js/vendor/jquery.min.js',
                'js/vendor/angular.min.js',
                'js/vendor/jquery.fullpage.min.js',
                ],
                dest: 'js/vendor.js'
            }
        },
        uglify: {
            dist: {
                src: '<%= concat.dist.dest %>',
                dest: 'scripts/vendor.min.js'
            },
            uglifyAngularApp: {
                src: 'js/main.js',
                dest: 'scripts/main.min.js'
            }
        },
        imagemin: {                          // Task 
            dynamic: {                         // Another target 
                files: [{
                    expand: true,                  // Enable dynamic expansion 
                    cwd: 'img-uncompressed/',                   // Src matches are relative to this path 
                    src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match 
                    dest: 'img/'                  // Destination path prefix 
                }]
            }
        }
    });

    // These plugins provide necessary tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    // Default task
    grunt.registerTask('default', ['concat', 'uglify', 'imagemin']);
};